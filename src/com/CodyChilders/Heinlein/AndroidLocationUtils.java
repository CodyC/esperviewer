package com.CodyChilders.Heinlein;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

public class AndroidLocationUtils
{
	public static GPSCoordinates GetLocation(Context mContext)
    {
		GPSCoordinates gpsCoordinates = new GPSCoordinates();
		
	     // Get the location manager
	     LocationManager locationManager 
	     	= (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
	     
	     Criteria criteria = new Criteria();
	     String bestProvider = locationManager.getBestProvider(criteria, false);
	     Location location = locationManager.getLastKnownLocation(bestProvider);
     
	     try 
	     { 
	    	 gpsCoordinates.setLatitude(location.getLatitude());
	    	 gpsCoordinates.setLongitude(location.getLongitude());
	     }
	     catch (NullPointerException e)
	     {
	    	 //This means we don't have GPS, just return a null GPSCoordinate object
	    	gpsCoordinates.setLatitude(null);
	    	gpsCoordinates.setLongitude(null);
	     }
	     
	     return gpsCoordinates; 
    }
}
