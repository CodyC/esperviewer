package com.CodyChilders.Heinlein;

//import org.apache.commons.codec.binary.Base64;

//import org.apache.commons.codec.binary.Base64;

import android.util.Base64;

/**
 * Created by Cody on 1/22/2015.
 */
public class Base64Utils {

    public static String encodeAsBase64String(String stringToEncode)
    {
        byte[] byteArrayToEncode = stringToEncode.getBytes();
        return Base64.encodeToString(byteArrayToEncode, 0);
    }
}
