package com.CodyChilders.Heinlein;

public class NumberParse<T extends Number>
{
	private Boolean ParseWasSuccessful;
	private T parseResult;

	public Boolean getParseWasSuccessful()
	{
		return ParseWasSuccessful;
	}
	public void setParseWasSuccessful(Boolean parseWasSuccessful)
	{
		ParseWasSuccessful = parseWasSuccessful;
	}
	
	public T getParseResult()
	{
		return parseResult;
	}
	public void setParseResult(T parseResult)
	{
		this.parseResult = parseResult;
	}
}
