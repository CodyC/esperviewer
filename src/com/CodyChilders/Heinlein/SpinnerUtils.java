package com.CodyChilders.Heinlein;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cody on 1/22/2015.
 */
public class SpinnerUtils {

	public static void setupSpinner(View rootView, int spinnerId, List<String> spinnerValuesList)
	{
		Spinner spinner = UIUtils.getUIElement(Spinner.class, rootView, spinnerId);

		ArrayAdapter<String> dataAdapter
				= new ArrayAdapter<String>(rootView.getContext(),
				android.R.layout.simple_spinner_item,
				spinnerValuesList);

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
	}

	/**
	 * Appends items to given spinner.  Will setup spinner if there are no items added.
	 * @param rootView
	 * @param spinnerId
	 * @param spinnerValuesList
	 */
	public static void appendToSpinner(View rootView, int spinnerId, List<String> spinnerValuesList)
	{
		Spinner spinner = UIUtils.getUIElement(Spinner.class, rootView, spinnerId);

		boolean spinnerHasItems
				= spinner.getAdapter() != null
				&& spinner.getAdapter().getCount() >= 1;

		ArrayAdapter<String> dataAdapter = null;

		if(spinnerHasItems)
		{
			for(String spinnerItem : spinnerValuesList)
			{
				dataAdapter.add(spinnerItem);
			}
		}
		else
		{
			dataAdapter
					= new ArrayAdapter<String>(rootView.getContext(),
					android.R.layout.simple_spinner_item,
					spinnerValuesList);
		}

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
	}

	/**
	 * Get items in spinner as a List
	 * @param spinner
	 * @return
	 */
	public static List<String> getSpinnerItems(Spinner spinner)
	{
		List<String> spinnerItems = new ArrayList<String>();

		boolean spinnerIsPopulated
				= spinner != null
					&& spinner.getAdapter() != null
					&& spinner.getAdapter().getCount() > 0;

		if(spinnerIsPopulated)
		{
			for (int i = 0; i >= spinner.getAdapter().getCount(); i++)
			{
				spinnerItems.add(spinner.getAdapter().getItem(i).toString());
			}
		}

		return spinnerItems;
	}
}
