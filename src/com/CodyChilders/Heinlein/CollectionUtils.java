package com.CodyChilders.Heinlein;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by Cody on 3/6/2015.
 */
public class CollectionUtils
{
	/**
	 * Removes dups in given list
	 * @param listToRemoveDups
	 * @return
	 * 	List with duplicate entries removed
	 */
	public static List<?> removeDuplicateItems(List<?> listToRemoveDups)
	{
		return new ArrayList<>(new LinkedHashSet<>(listToRemoveDups));
	}

	/**
	 * Concatenates all lists passed in.  Adds in the order passed... almost if they were concatenating.....
	 * @param listToConcat
	 * @return
	 */
	public static <T> List<T> concatLists(List<T>... listToConcat)
	{
		List<T> concatenatedList = new ArrayList<>();

		for(List<T> currentList : listToConcat)
		{
			if(currentList != null && currentList.size() > 0)
			{
				concatenatedList.addAll(currentList);
			}
		}

		return concatenatedList;
	}


//	/**
//	 * Get subset of all lists passed in.
//	 * @param listToConcat
//	 * @return
//	 */
//	public static <T> List<T> getSubset(List<T>... listToCompare)
//	{
//		List<T> subsetList = new ArrayList<>();
//
//		for(List<T> currentList : listToCompare)
//		{
//			if(currentList != null && currentList.size() > 0)
//			{
//				for(T currentItem : currentList)
//				{
//					if()
//				}
//			}
//		}
//
//		return subsetList;
//	}

}
