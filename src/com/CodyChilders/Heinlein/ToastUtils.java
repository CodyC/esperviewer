package com.CodyChilders.Heinlein;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Cody on 2/16/2015.
 */
public class ToastUtils
{
    public static final int DOUBLE_LONG_TOAST_LENGTH = Toast.LENGTH_LONG * 2;

    public static void showLongToast(Context context, String message)
    {
        showToast(context, message, Toast.LENGTH_LONG);
    }

	public static void showDoubleLongToast(Context context, String message)
	{
		showToast(context, message, DOUBLE_LONG_TOAST_LENGTH);
	}

    public static void showShortToast(Context context, String message)
    {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void showToast(Context context, String message, int duration)
    {
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

}
