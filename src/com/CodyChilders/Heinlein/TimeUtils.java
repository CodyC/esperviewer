package com.CodyChilders.Heinlein;

import android.widget.TimePicker;

import java.util.Calendar;

public class TimeUtils
{
	public static Double getCurrentUnixTimeStamp()
	{
		return Double.valueOf(System.currentTimeMillis()/1000);
	}

	public static Double getUnixTimeStamp(Calendar calendar)
	{
	return new Double(calendar.getTimeInMillis() / 1000L);
	}

	public static Double getUnixTimestamp(TimePicker timePicker)
	{
		Integer currentHour = timePicker.getCurrentHour();
		Integer currentMinute = timePicker.getCurrentMinute();

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.HOUR_OF_DAY, currentHour);
		calendar.set(Calendar.MINUTE, currentMinute);

//		String am_pm = "";
//		if (pickupCalendar.get(Calendar.AM_PM) == Calendar.AM)
//		{
//			am_pm = "AM";
//		}
//		else if (pickupCalendar.get(Calendar.AM_PM) == Calendar.PM)
//		{
//			am_pm = "PM";
//		}

		return TimeUtils.getUnixTimeStamp(calendar);
	}
}
