package com.CodyChilders.Heinlein;

import android.app.Fragment;
import android.app.FragmentManager;

/**
 * Created by Cody on 2/10/2015.
 */
public class FragmentUtils
{

    public static void switchCurrentFragment(Fragment fragmentInstance, FragmentManager fragmentManager, int containerId)
    {
        fragmentManager
                .beginTransaction()
                .replace(containerId, fragmentInstance)
                .commit();
    }
}
