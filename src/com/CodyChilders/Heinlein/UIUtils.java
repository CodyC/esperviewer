package com.CodyChilders.Heinlein;

import android.view.View;

/**
 * Created by Cody on 1/22/2015.
 */
public class UIUtils {

    public static <T> T getUIElement(Class<T> uiElementType, View rootView, int elementId){

        T uiElement;

        try
        {
            Object uiElementToCast = rootView.findViewById(elementId);
            uiElement = uiElementType.cast(uiElementToCast);
        }catch(Exception exc)
        {
            uiElement = null;
        }

        return uiElement;
    }

}
