package com.CodyChilders.Heinlein;

import android.app.Activity;
import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Cody on 3/3/2015.
 */
public class FileUtils
{
	/**
	 * Writes the given string to the specified file name.  This method defaults to MODE_PRIVATE write
	 * @param activity
	 * @param filename
	 * @param dataToWrite
	 * @throws IOException
	 */
	public static void writeStringToFile(Activity activity, String filename, String dataToWrite) throws IOException
	{
		File storeFile = new File(activity.getFilesDir(), filename);

		createFileIfNotExists(storeFile);

		FileOutputStream 	outputStream = activity.openFileOutput(filename, Context.MODE_PRIVATE);

		outputStream.write(dataToWrite.getBytes());
		outputStream.close();
	}

	/**
	 * Creates file if the specified file doesn't exist
	 * @param storeFile
	 * @return boolean to indicate if file was created.
	 */
	public static boolean createFileIfNotExists(File storeFile)
	{

		boolean fileWasCreated = false;

		if(!storeFile.exists())
		{
			try
			{
				storeFile.createNewFile();
				fileWasCreated = true;
			}
			catch (Exception e)
			{
				fileWasCreated = false;
			}
		}

		return fileWasCreated;
	}

	/**
	 * Returns list of filenames in the given directory.
	 * @param activity
	 * @param relativeFilePath
	 * @return
	 */
	public static List<String> getFilesInDirectory(Activity activity, String relativeFilePath)
	{
		List<String> fileNameList = new ArrayList<>();
		File fileDirToFind = new File(activity.getFilesDir(), relativeFilePath);

		if(fileDirToFind.list() !=null)
		{
			fileNameList = Arrays.asList(fileDirToFind.list());
		}

		return fileNameList;
	}

	/**
	 * Returns contents of file as a string
	 * @param activity
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String readFromFile(Activity activity, String fileName) throws IOException
	{
		BufferedReader inputReader
			= new BufferedReader(new InputStreamReader(activity.openFileInput(fileName)));

		String inputString;

		StringBuffer stringBuffer = new StringBuffer();

		while ((inputString = inputReader.readLine()) != null)
		{
			stringBuffer.append(inputString + StringUtils.NEW_LINE);
		}

		return stringBuffer.toString();
	}
}
