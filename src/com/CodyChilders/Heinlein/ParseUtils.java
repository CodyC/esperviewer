package com.CodyChilders.Heinlein;

public class ParseUtils
{
	
	 public static <T extends Number> NumberParse tryParse(String valueToParse,
                                                                Class<T> numberClass)
	 {
		 NumberParse integerParse = new NumberParse();

		 try
		 {
             Number parsedNumber = null;

             if(numberClass.equals(Byte.class))
             {
                 parsedNumber = Byte.valueOf(valueToParse);
             }
             else if(numberClass.equals(Integer.class))
             {
                 parsedNumber = Integer.valueOf(valueToParse);
             }
             else if(numberClass.equals(Double.class))
             {
                 parsedNumber = Double.valueOf(valueToParse);
             }
             else if(numberClass.equals(Float.class))
             {
                 parsedNumber = Float.valueOf(valueToParse);
             }
             else if(numberClass.equals(Long.class))
             {
                 parsedNumber = Long.valueOf(valueToParse);
             }
             else
             {
                 throw new IllegalArgumentException();
             }

			 integerParse.setParseResult(parsedNumber);

			 integerParse.setParseWasSuccessful(true);
		 }
		 catch(Exception exc)
		 {
			 integerParse.setParseResult(null);
			 integerParse.setParseWasSuccessful(false);
		 }
		 
		 return integerParse;
	 }
}
