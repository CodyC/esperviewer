package com.CodyChilders.Heinlein;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Cody on 12/19/2015.
 */
public class AnimationsCollection {


    public static List<Animation> getAnimations(Context context) {

        return Arrays.asList(
             AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left),
             AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right),
             AnimationUtils.loadAnimation(context, android.R.anim.fade_in),
             AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
             //AnimationUtils.loadAnimation(context, android.R.anim.accelerate_decelerate_interpolator),
             //AnimationUtils.loadAnimation(context, android.R.anim.accelerate_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.anticipate_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.anticipate_overshoot_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.bounce_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.cycle_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.linear_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.overshoot_interpolator),
//             AnimationUtils.loadAnimation(context, android.R.anim.linear_interpolator)
            );

    }
}
