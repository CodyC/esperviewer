package com.CodyChilders.Heinlein;

import android.app.Activity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class JSONFileUtils
{
	//public static final String DEFAULT_JSON_FILE_NAME = "wasteWaterRecords.json";
	
	
	public static void saveToFile(String jsonStringToSave, String fileName, Activity activity) throws IOException
	{
		FileOutputStream outputStream;
		File testFile = new File(fileName);
		
		//Delete the old file
		testFile.delete();
				
		//save the new one
		int privateWriteMode = 0;

		outputStream = activity.openFileOutput(fileName, privateWriteMode);
		outputStream.write(jsonStringToSave.getBytes());
		outputStream.close();
	}
	
	
	public static String readFromFile(String fileName, Activity activity) throws IOException
	{
		try 
		{
		    BufferedReader inputReader 
		    	= new BufferedReader(new InputStreamReader(activity.openFileInput(fileName)));

		    String inputString;

		    StringBuffer stringBuffer = new StringBuffer();                

		    while ((inputString = inputReader.readLine()) != null) 
			{
		        stringBuffer.append(inputString + "\n");
		    }
		    
		    return stringBuffer.toString();
		}
		catch (IOException e) 
		{
		    throw e;
		}
	}
}
