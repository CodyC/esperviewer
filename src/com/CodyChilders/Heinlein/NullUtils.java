package com.CodyChilders.Heinlein;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Cody on 2/16/2015.
 */
public class NullUtils
{

    /**
     * Check all arguments to see if they are null.
     * Returns true if a single null is found, returns false if all are not null.
     * @param itemsToNullCheck
     * @return
     */
    public static Boolean areObjectsNull(Object... itemsToNullCheck)
    {
        Boolean isNull = false;

        for(Object currentItem : itemsToNullCheck)
        {
            isNull = currentItem == null;

            if(isNull)
            {
                break; //we have found a null exit
            }
        }

        return isNull;
    }


    /**
     * Check all arguments to see if they are empty.
     * Returns true if a single empty is found, returns false if all are not empty.
     * @param stringsToEmptyCheck
     * @return
     */
    public static Boolean areStringsEmpty(String... stringsToEmptyCheck)
    {
        Boolean isEmpty = false;

        for(String currentString : stringsToEmptyCheck)
        {
            isEmpty = StringUtils.isEmpty(currentString);

            if(isEmpty)
            {
                break; //we have found a null exit
            }
        }

        return isEmpty;
    }
}
