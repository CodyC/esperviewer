package com.CodyChilders.Heinlein;

import java.text.DecimalFormat;

public class GPSCoordinates
{
	private Double Latitude;
	public Double getLatitude()
	{
		return Latitude;
	}

	public void setLatitude(Double latitude)
	{
		Latitude = latitude;
	}

	private Double Longitude;
	public Double getLongitude()
	{
		return Longitude;
	}

	public void setLongitude(Double longitude)
	{
		Longitude = longitude;
	}
	
	public float getLongitudeFloat()
	{
		return (float)Longitude.doubleValue();
	}
	
	public float getLatitudeFloat()
	{		
		return (float)Latitude.doubleValue();
	}
	
	private static final DecimalFormat df = new DecimalFormat("##.####");
	
	public String buildDisplayableLocation()
	{	
		
		if(this.Latitude != null
			&&
		   this.Longitude != null)
		{
		
			String locationDisplay 
				= df.format(this.Latitude) 
				+ " " 
				+ df.format(this.Longitude);
			
			return locationDisplay;
		}
		else
		{
			return "Can't retreive GPS signal";
		}
	}
}
