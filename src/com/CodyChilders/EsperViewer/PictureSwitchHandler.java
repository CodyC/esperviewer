package com.CodyChilders.EsperViewer;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.widget.ImageSwitcher;
import org.apache.commons.lang3.RandomUtils;

import java.util.List;
import java.util.Set;

/**
 * Created by Cody on 12/17/2015.
 */
public class PictureSwitchHandler
{

	public static final int MAX_IMAGE_HEIGHT = 4000;
	public static final int MAX_IMAGE_WIDTH = 4000;
	public static final int MINIMUM_WAIT_TIME_IN_MILLISECONDS = 100_000;
	public static final int MAXIMUM_WAIT_TIME_IN_MILLISECONDS = 300_000;

	private List<Uri> imageUrisToDisplay;

	public List<Uri> getImageUrisToDisplay()
	{
		return imageUrisToDisplay;
	}

	public void setImageUrisToDisplay(List<Uri> imageUrisToDisplay)
	{
		this.imageUrisToDisplay = imageUrisToDisplay;
	}

	public void setHandler(android.os.Handler handler)
    {
        this.handler = handler;
    }

    private android.os.Handler handler;

    public ImageSwitcher getImageSwitcher()
    {
        return imageSwitcher;
    }

    public void setImageSwitcher(ImageSwitcher imageSwitcher)
    {
        this.imageSwitcher = imageSwitcher;
    }

    private ImageSwitcher imageSwitcher;

    public Context getContext()
    {
        return context;
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    private Context context;

    public Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {

	        showRandomPicture();

			//Mix up the wait time a little bit
	        final int pitchSwitchTimeInMilliseconds =
			        RandomUtils.nextInt(MINIMUM_WAIT_TIME_IN_MILLISECONDS, MAXIMUM_WAIT_TIME_IN_MILLISECONDS);

	        //Call the handler again in the given interval
	        handler.postDelayed(this, pitchSwitchTimeInMilliseconds);
        }
    };

	public void showRandomPicture()
	{
		Uri picUri = getRandomPicUri();

		Bitmap randomBitmap = BitmapFactory.decodeFile(picUri.getPath());

		//When we hit images that are too big, the viewer just shows a black screen,
		// so we need to scale them down.
		if(randomBitmap.getHeight() >= MAX_IMAGE_WIDTH || randomBitmap.getWidth() >= MAX_IMAGE_HEIGHT)
		{

			final int scaleFactor;

			if(randomBitmap.getHeight() >= randomBitmap.getWidth())
			{
				scaleFactor = randomBitmap.getHeight() / 2000;
			}
			else
			{
				scaleFactor = randomBitmap.getWidth() / 2000;
			}

			final int scaledHeight = randomBitmap.getHeight() / scaleFactor;
			final int scaledWidth = randomBitmap.getWidth() / scaleFactor;

			Bitmap scaledBitmap = Bitmap.createScaledBitmap(randomBitmap, scaledWidth, scaledHeight, false);
			BitmapDrawable scaledDrawable = new BitmapDrawable(scaledBitmap);

			imageSwitcher.setImageDrawable(scaledDrawable);
		}
		else
		{
			imageSwitcher.setImageURI(picUri);
		}
	}

	public Uri getRandomPicUri() {
		final int nextPicIndex = RandomUtils.nextInt(0, imageUrisToDisplay.size());

		return imageUrisToDisplay.get(nextPicIndex);
	}

}
