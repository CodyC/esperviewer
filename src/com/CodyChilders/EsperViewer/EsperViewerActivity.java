package com.CodyChilders.EsperViewer;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.List;

public class EsperViewerActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Keep the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Base stolen from: http://developer.android.com/training/system-ui/navigation.html
        //and http://developer.android.com/training/system-ui/immersive.html
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        //This should match with the tablet pretty well, unless it is a darker black...
        //https://youtu.be/4IUNc6yxp2g
        decorView.setBackgroundColor(Color.BLACK);

        //getActionBar().hide();

        //Remove notification bar
        this.getWindow()
                .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                          WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.main);

        final ImageSwitcher photoDisplayIS = (ImageSwitcher) findViewById(R.id.photoDisplayIS);

        Animation fadeIn =
                AnimationUtils.loadAnimation(this, android.R.anim.fade_in);

        Animation fadeOut =
                AnimationUtils.loadAnimation(this, android.R.anim.fade_out);

        photoDisplayIS.setInAnimation(fadeIn);
        photoDisplayIS.setOutAnimation(fadeOut);

        photoDisplayIS.setFactory(photoDisplayISViewFactory);

	    Handler handler = new Handler();

	    PictureSwitchHandler picSwitchHandler = new PictureSwitchHandler();

	    List<Uri> imageUris = getImageUris();

	    picSwitchHandler.setImageUrisToDisplay(imageUris);
        picSwitchHandler.setHandler(handler);
        picSwitchHandler.setImageSwitcher(photoDisplayIS);
        picSwitchHandler.setContext(this);

	    // show an image as we first load
		picSwitchHandler.showRandomPicture();

        handler.postDelayed(picSwitchHandler.runnable, 0);
    }

	private List<Uri> getImageUris()
	{

		Cursor imageCursor = this.getContentResolver()
				.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
						null,
						null,
						null,
						MediaStore.Images.Media.DEFAULT_SORT_ORDER);

		List<Uri> imageUris = new ArrayList<>();

		final int PICTURE_URI_COLUMN_INDEX = 1;

		while(imageCursor.moveToNext())
		{
			final String currentPicPath = imageCursor.getString(PICTURE_URI_COLUMN_INDEX);
			imageUris.add(Uri.parse(currentPicPath));
		}

		imageCursor.close();

		return imageUris;
	}

	ViewSwitcher.ViewFactory photoDisplayISViewFactory = new ViewSwitcher.ViewFactory()
    {
        @Override
        public View makeView() {

	        ImageView imageView = new ImageView(EsperViewerActivity.this);
	        imageView.setAdjustViewBounds(true);

	        //This seems to work well with scaling the image, while maintaining the aspect ratio.
	        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

	        FrameLayout.LayoutParams params =
	                new ImageSwitcher.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT,
			                                         FrameLayout.LayoutParams.FILL_PARENT);

	        imageView.setLayoutParams(params);

	        return imageView;
        }
    };

}
